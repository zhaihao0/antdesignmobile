const routesList = {
  routes: [
    { path: '/', component: './index.js' },
    { path: '/login', component: './Login'},
    { path: '/family', component: '../Family'},
    { path: '/award', component: './Award'},
    { path: '/personage', component: './Personage'},
    { path: '/punishment', component: './Punishment'},
    { path: '/familyEdit', component: './FamilyEdit'},
    { path: '/familyAdd', component: './FamilyAdd'},
    { path: '/experience', component: './Experience'},
    { path: '/experienceAdd', component: './ExperienceAdd'},
    { path: '/experienceEdit', component: './ExperienceEdit'},
    { path: '/newsView', component: './NewsView'},
    { path: '/vacate', component: './Vacate'},
    { path: '/vacateAdd', component: './VacateAdd'},
    { path: '/vacateView', component: './VacateView'},
    { path: '/attendance', component: './Attendance'},
    { path: '/schoolReport', component: './SchoolReport'},
    { path: '/classMute', component: './ClassMute'},
    { path: '/classInfo', component: './ClassInfo'},
    { path: '/messagedill', component: './MessageDill'},
    { path: '/messagedillview', component: './MessageDillView'},
  ],
}
