import  './index.less';
import { TransitionGroup, CSSTransition } from "react-transition-group";


function BasicLayout(props) {
  const {location} = props
  return (

    <TransitionGroup>
      <CSSTransition
        // 需要加一个key属性，让react认识每个组件，并进行正确的加载。
        // 这里我改了官方demo的代码， 原来是设置成location.key， 这样的话每次点击同一个路由链接的时候都会渲染。
        key={location.pathname}
        // classNames 就是设置给css动画的标示，记得'classNames'带's'的。
        classNames="fade"
        // 动画时间设置为800ms，和css中的需要一致。
        timeout={800}
      >
        <div className='normal'>
          { props.children }
        </div>
      </CSSTransition>
    </TransitionGroup>

  );
}

export default BasicLayout;
