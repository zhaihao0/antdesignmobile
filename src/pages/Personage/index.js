import React from 'react'
import http from '@/utils/http';
import { NavBar, Icon, TextareaItem, List, InputItem, WingBlank, Button, WhiteSpace, Toast } from 'antd-mobile';
import router from 'umi/router';
import { createForm, formShape } from 'rc-form';
import BaseSelect from '@/components/BaseSelect';
import SelectTypeCode from '@/components/SelectTypeCode';

const Item = List.Item;

// 个人信息
class Personage extends React.Component{


  state = {
    zzmmDesc:'',
    mzDesc: '',
    zzmm:'',
    mzdm:''
  }

  componentDidMount () {
    const {query:{userInfo}} = this.props.location
    this.setState({
        zzmmDesc:userInfo.zzmmDesc || '',
        zzmm:userInfo.zzmm || '',
        mzDesc:userInfo.mzDesc || '',
        mzdm:userInfo.mzdm || '',
      }
    )
  }

  zzmmChange = val => {
    this.setState({
      zzmmDesc:val.codeDesc,
      zzmm:val.code
     }
    )
  }

  mzChange = val => {
    this.setState({
      mzDesc:val.codeDesc,
      mzdm:val.code
      }
    )
  }

  postData = obj => {
    http.post('/api/bus/savexsxx',{...obj}).then(res => {
      if(res.data.code === '0') {
        Toast.success('保存成功', 1);
        router.replace({
          pathname:'/'
        })
      }
    })
  }

  onSubmit = () => {
    const {zzmm, mzdm} = this.state
    this.props.form.validateFields({ force: true }, (error) => {
      if (!error) {
        const t = this.props.form.getFieldsValue()
        t.zzmm = zzmm
        t.mzdm = mzdm
        this.postData(t)
      } else {
        Toast.fail('请检查是否有必填项未填!');
      }
    });
  }

  renderInfoList = () => {
    let errors;
    const { getFieldProps, getFieldError } = this.props.form;
    const {query:{userInfo}} = this.props.location
    const {zzmmDesc, mzDesc} = this.state
    return (
      <>
        <form>
          <List>
            <Item extra={userInfo.xm || ''}>姓名</Item>
            <Item extra={userInfo.xb || ''}>性别</Item>
            <Item extra={userInfo.xh || ''}>学号</Item>
            <InputItem
              {...getFieldProps('lxdh', {
                initialValue: userInfo.lxdh || '',
              })}
              clear
              error={!!getFieldError('lxdh')}
              onErrorClick={() => {
                alert(getFieldError('lxdh').join('、'));
              }}
              placeholder="请输入联系电话"
            >
              <span>联系电话</span>
            </InputItem>
            <InputItem
              {...getFieldProps('sfzh', {
                initialValue: userInfo.sfzh || '',
              })}
              clear
              error={!!getFieldError('sfzh')}
              onErrorClick={() => {
                alert(getFieldError('sfzh').join('、'));
              }}
              placeholder="请输入身份证号"
            >
              <span>身份证号</span>
            </InputItem>
            <InputItem
              {...getFieldProps('qq', {
                initialValue: userInfo.qq || '',
              })}
              clear
              error={!!getFieldError('qq')}
              onErrorClick={() => {
                alert(getFieldError('qq').join('、'));
              }}
              placeholder="请输入QQ"
            >
              <span>QQ</span>
            </InputItem>
            <InputItem
              {...getFieldProps('dzyj', {
                initialValue: userInfo.dzyj || '',
              })}
              clear
              error={!!getFieldError('dzyj')}
              onErrorClick={() => {
                alert(getFieldError('dzyj').join('、'));
              }}
              placeholder="请输入邮箱"
            >
              <span>邮箱</span>
            </InputItem>
            <InputItem
              {...getFieldProps('yzbm', {
                initialValue: userInfo.yzbm || '',
              })}
              clear
              error={!!getFieldError('yzbm')}
              onErrorClick={() => {
                alert(getFieldError('yzbm').join('、'));
              }}
              placeholder="请输入邮政编码"
            >
              <span>邮政编码</span>
            </InputItem>
            <TextareaItem
              title="家庭地址"
              {...getFieldProps('jtdz', {
                initialValue: userInfo.jtdz || '',
              })}
              clear
              error={!!getFieldError('jtdz')}
              onErrorClick={() => {
                alert(getFieldError('jtdz').join('、'));
              }}
              placeholder="请输入家庭地址"
            >
              <span>家庭地址</span>
            </TextareaItem>
            <BaseSelect value={zzmmDesc} onChage={this.zzmmChange} label={<span>政治面貌</span>} typeCode={SelectTypeCode.zhengzhimianmao} />
            <BaseSelect value={mzDesc} onChage={this.mzChange} label={<span>民族</span>} typeCode={SelectTypeCode.minzu} />
          </List>
        </form>
        <WingBlank>
          <Button type="primary" style={{marginTop:30}}   onClick={this.onSubmit}>保存</Button>
        </WingBlank>
      </>
    )
  }

  render () {
    return (
      <div>
        <NavBar
          mode="light"
          icon={<Icon type="left" />}
          onLeftClick={() => {router.go(-1)}}
          style={{
            borderBottom: '0.5px solid #eee'
          }}
        >个人信息</NavBar>
        {this.renderInfoList()}
      </div>
    )
  }

}

export default createForm()(Personage);
