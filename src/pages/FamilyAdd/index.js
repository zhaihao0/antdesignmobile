import React from 'react'
import { createForm, formShape } from 'rc-form';
import { Icon, NavBar, List, InputItem,WingBlank, Button, Toast } from 'antd-mobile';
import router from 'umi/router';
import BaseSelect from '@/components/BaseSelect'
import http from '@/utils/http'
import SelectTypeCode from '@/components/SelectTypeCode';

const Item = List.Item;

class FamilyAdd extends React.Component{

  static propTypes = {
    form: formShape,
  };

  state = {
    value: 1,
    jzgx:null,
    jzzzmm: null,
    jzgxcode: null,
    jzzzmmcode: null
  }

  jtgxChange = val => {
    this.setState({
      jzgx:val.codeDesc,
      jzgxcode:val.code
    })
  }

  zzmmChange = val => {
    this.setState({
      jzzzmm:val.codeDesc,
      jzzzmmcode:val.code
    })
  }

  postData = obj => {
    http.post('/api/bus/saveJtxx',{...obj}).then(res => {
      if(res.data.code === '0') {
        Toast.success('保存成功', 1);
        router.replace({
          pathname:'/family'
        })
      }
    })
  }

  onSubmit = () => {
    const {jzgxcode, jzzzmmcode} = this.state
    this.props.form.validateFields({ force: true }, (error) => {
      if (!error) {
        const t = this.props.form.getFieldsValue()
        t.jzgx = jzgxcode
        t.jzzzmm = jzzzmmcode
        if(jzgxcode && jzzzmmcode ) {
          console.log(jzgxcode, jzzzmmcode)
          this.postData(t)
        } else {
          Toast.fail('请检查是否有必填项未填!');
        }
      } else {
        Toast.fail('请检查是否有必填项未填!');
      }
    });
  }

  onReset = () => {
    this.props.form.resetFields();
    this.setState({
      jzgx: null,
      jzzzmm: null
    })
  }


  validateJZXM = (rule, value, callback) => {
    if (value && value.length >= 0) {
      callback();
    } else {
      callback(new Error('家长姓名是必填项'));
    }
  }

  validateJZZW = (rule, value, callback) => {
    if (value && value.length >= 0) {
      callback();
    } else {
      callback(new Error('家长职务是必填项'));
    }
  }

  componentDidMount () {
  }


  render () {
    let errors;
    const { getFieldProps, getFieldError } = this.props.form;
    const {jzgx, jzzzmm} = this.state
    return (
      <div>
        <NavBar
          mode="light"
          icon={<Icon type="left" />}
          onLeftClick={() => {router.go(-1)}}
          style={{
            borderBottom: '0.5px solid #eee'
          }}
        >家庭信息添加</NavBar>
        <form>
          <List
            renderFooter={() => getFieldError('jzxm') && getFieldError('jzxm').join(',')}
          >
            <InputItem
              {...getFieldProps('jzxm', {
                // initialValue: 'little ant',
                rules: [
                  { required: true, message: '请输入家长姓名' },
                  { validator: this.validateJZXM },
                ],
              })}
              clear
              error={!!getFieldError('jzxm')}
              onErrorClick={() => {
                alert(getFieldError('jzxm').join('、'));
              }}
              placeholder="请输入家长姓名"
            ><span>家长姓名<span style={{color:'red'}}>*</span></span></InputItem>
            <InputItem
              {...getFieldProps('jzzw', {
                // initialValue: 'little ant',
                rules: [
                  { required: true, message: '请输入家长职务' },
                  { validator: this.validateJZZW },
                ],
              })}
              clear
              error={!!getFieldError('jzzw')}
              onErrorClick={() => {
                alert(getFieldError('jzzw').join('、'));
              }}
              placeholder="请输入家长职务"
            ><span>家长职务<span style={{color:'red'}}>*</span></span></InputItem>
            <InputItem
              {...getFieldProps('jzsfzh', {
                // initialValue: 'little ant',
                rules: [
                  { required: true, message: '请输入家长身份证号' },
                ],
              })}
              clear
              error={!!getFieldError('jzsfzh')}
              onErrorClick={() => {
                alert(getFieldError('jzsfzh').join('、'));
              }}
              placeholder="请输入家长身份证号"
            ><span>身份证号<span style={{color:'red'}}>*</span></span></InputItem>
            <InputItem
              {...getFieldProps('jzdh', {
                // initialValue: 'little ant',
                rules: [
                  { required: true, message: '请输入家长电话' },
                ],
              })}
              clear
              error={!!getFieldError('jzdh')}
              onErrorClick={() => {
                alert(getFieldError('jzdh').join('、'));
              }}
              placeholder="请输入家长电话"
            ><span>家长电话<span style={{color:'red'}}>*</span></span></InputItem>
            <BaseSelect value={jzzzmm} onChage={this.zzmmChange} label={<span>政治面貌<span style={{color:'red'}}>*</span></span>} typeCode={SelectTypeCode.zhengzhimianmao} />
            <BaseSelect value={jzgx} onChage={this.jtgxChange} label={<span>家庭关系<span style={{color:'red'}}>*</span></span>} typeCode={SelectTypeCode.jiatingguanxi} />
          </List>
        </form>
        <WingBlank>
          <Button type="primary"   onClick={this.onSubmit}>保存</Button>
        </WingBlank>
      </div>
    )
  }

}

export default createForm()(FamilyAdd);
