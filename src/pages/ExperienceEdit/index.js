import React from 'react'
import { createForm, formShape } from 'rc-form';
import { Icon, NavBar, List, InputItem,WingBlank, Button, Toast, DatePicker, TextareaItem } from 'antd-mobile';
import router from 'umi/router';
import moment from 'moment'
import http from '@/utils/http'
import SelectTypeCode from '@/components/SelectTypeCode';

const Item = List.Item;

class ExperienceEdit extends React.Component{

  static propTypes = {
    form: formShape,
  };

  state = {
    value: 1,
    qsrq:moment(new Date()).format('YYYY-MM-DD'),
    jsrq:moment(new Date()).format('YYYY-MM-DD'),
  }


  postData = obj => {
    http.post('/api/bus/svaeJl',{...obj}).then(res => {
      if(res.data.code === '0') {
        Toast.success('保存成功', 1);
        router.replace({
          pathname:'/experience'
        })
      }
    })
  }

  onSubmit = () => {
    const {qsrq, jsrq} = this.state
    const {location:{query}} = this.props
    this.props.form.validateFields({ force: true }, (error) => {
      if (!error) {
        const t = this.props.form.getFieldsValue()
        t.qsrq = qsrq
        t.jsrq = jsrq
        t.id = query.id
        if(qsrq && jsrq) {
          this.postData(t)
        } else {
          Toast.fail('请检查是否有必填项未填!');
        }
      } else {
        Toast.fail('请检查是否有必填项未填!');
      }
    });
  }

  onReset = () => {
    this.props.form.resetFields();
  }



  componentDidMount () {
    const {query} = this.props.location
    this.setState({
      qsrq:query.qsrq,
      jsrq:query.jsrq
    })
  }


  render () {
    let errors;
    const { getFieldProps, getFieldError } = this.props.form;
    const {qsrq, jsrq} = this.state
    const {query} = this.props.location
    return (
      <div>
        <NavBar
          mode="light"
          icon={<Icon type="left" />}
          onLeftClick={() => {router.go(-1)}}
          style={{
            borderBottom: '0.5px solid #eee'
          }}
        >个人经历编辑</NavBar>
        <form>
          <List>
            <DatePicker
              mode="date"
              title="Select Date"
              extra={qsrq}
              value={this.state.date}
              onChange={date => this.setState({ qsrq:moment(date).format('YYYY-MM-DD') })}
            >
              <List.Item arrow="horizontal"><span>起始日期<span style={{color:'red'}}>*</span></span></List.Item>
            </DatePicker>
            <DatePicker
              mode="date"
              title="Select Date"
              extra={jsrq}
              value={this.state.date}
              onChange={date => this.setState({ jsrq:moment(date).format('YYYY-MM-DD')  })}
            >
              <List.Item arrow="horizontal"><span>结束日期<span style={{color:'red'}}>*</span></span></List.Item>
            </DatePicker>
            <TextareaItem
              {...getFieldProps('hdgzxx', {
                initialValue:query.hdgzxx,
                rules: [
                  { required: true, message: '请输入何地工作或学习' },
                ],
              })}
              clear
              error={!!getFieldError('hdgzxx')}
              onErrorClick={() => {
                alert(getFieldError('hdgzxx').join('、'));
              }}
              title={<span>内容描述<span style={{color:'red'}}>*</span></span>}
              placeholder="请输入在何地工作或学习"
              data-seed="logId"
              autoHeight
            />
            <InputItem
              {...getFieldProps('zmr', {
                initialValue:query.zmr,
                rules: [
                  { required: true, message: '请输入证明人' },
                ],
              })}
              clear
              error={!!getFieldError('zmr')}
              onErrorClick={() => {
                alert(getFieldError('zmr').join('、'));
              }}
              placeholder="请输入证明人"
            ><span>证明人<span style={{color:'red'}}>*</span></span></InputItem>

          </List>
        </form>
        <WingBlank>
          <Button type="primary"  style={{marginTop:20}}  onClick={this.onSubmit}>保存</Button>
        </WingBlank>
      </div>
    )
  }

}

export default createForm()(ExperienceEdit);
