import React from 'react'
import http from '@/utils/http';
import {NavBar, Icon,  Card, WhiteSpace, Modal, Toast  } from 'antd-mobile';
import router from 'umi/router';
import {stringify} from 'qs'
import styles from './index.less'

const alert = Modal.alert;

class Family extends React.Component{

  state = {
    familyList: []
  }


  componentDidMount () {
    this.fetchData()
  }

  fetchData = () => {
    http(`/api/mlogin/xsJtxx`).then(res => {
      console.log(res.data)
      if(res.data){
        this.setState({
          familyList:res.data
        })
      }
    })
  }

  del = (e,obj) =>{
    e.stopPropagation()
    alert('删除', '确认删除吗?', [
      { text: '取消', onPress: () => {} },
      { text: '确认', onPress: () => {
        http(`/api/bus/delJtxx?${stringify({id:obj.id})}`).then(res => {
          if(res.data.code === '0') {
            Toast.success('删除成功')
            this.fetchData()
          }
        })
        }},
    ])
  }

  edit = (e,obj) => {
    e.stopPropagation()
    router.push(
      {
        pathname:'/familyEdit',
        query:obj
      }
    )
  }

  add = () => {
    router.push(
      {
        pathname:'/familyAdd',
      }
    )
  }

  cardActions = (obj) => {
    const inStyles = {
      fontSize:12,
      marginLeft:15,
    }
    return (
      <>
        <span className={styles.buttons} style={{color:'red', ...inStyles}} onClick={(e) => {this.del(e,obj)}}>删除</span>
        <span className={styles.buttons} style={{color:'#168ee9', ...inStyles}} onClick={(e) => {this.edit(e,obj)}} >编辑</span>
      </>
    )
  }

  renderList = () => {
    const {familyList} = this.state
    return (
      familyList.map(t => {
        return (
          <div key={t.id}>
            <WhiteSpace size="lg" />
            <Card full>
              <Card.Header
                title={`${t.jzxm} - ${t.jzgx}`}
                extra={this.cardActions(t)}
              />
              <Card.Body>
                <div>联系电话: {t.jzdh}</div>
              </Card.Body>
              <Card.Footer content={t.jzzzmm} extra={t.jzzw} />
            </Card>
          </div>
        )
      })
    )
  }


  render () {
    return (
      <div>
        <NavBar
          mode="light"
          icon={<Icon type="left" />}
          onLeftClick={() => {router.go(-1)}}
          style={{
            borderBottom: '0.5px solid #eee'
          }}
          rightContent={[
           <span key={0} onClick={() => {this.add()}} style={{fontSize:14}}>添加</span>,
          ]}
        >家庭信息</NavBar>
        {this.renderList()}
      </div>
    )
  }

}

export default Family
