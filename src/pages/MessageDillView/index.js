import React from 'react'
import CommonFlowDisplay from '@/components/CommonFlowDisplay'
import StepsInfo from '@/components/StepsInfo'
import { Icon, NavBar, Modal, } from 'antd-mobile';
import router from 'umi/router';

class MessageDillView extends React.Component{

  render () {
    const { id } = this.props.location.query
    return (
      <div>
        <NavBar
          mode="light"
          icon={<Icon type="left" />}
          onLeftClick={() => {router.go(-1)}}
          style={{
            borderBottom: '0.5px solid #eee'
          }}
        >已办详细</NavBar>
        <CommonFlowDisplay url={`/api/bus/spck?proccessId=${id}`} />
        <StepsInfo url={`/api/bus/spckmx?proccessId=${id}`} />
      </div>
    )
  }

}

export default MessageDillView
