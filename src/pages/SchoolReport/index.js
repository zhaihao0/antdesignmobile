import React from 'react'
import router from 'umi/router'
import { NavBar, Icon,  Card, WhiteSpace  } from 'antd-mobile';
import http from '@/utils/http'

class SchoolReport extends React.Component{

  state = {
    reportList: [
      {
        kcmc:'高数',
        fs:'80',
        xq:'上学期',
        xn:'2018',
        id:0
      },
      {
        kcmc:'流体力学',
        fs:'40',
        xq:'上学期',
        xn:'2018',
        id:1
      },
      {
        kcmc:'大学英语',
        fs:'40',
        xq:'上学期',
        xn:'2018',
        id:2
      },
    ]
  }


  componentDidMount () {
    // todo 成绩查询接口
    // http(`/api/mlogin/xsJlxx`).then(res => {
    //   console.log(res.data)
    //   if(res.data){
    //     this.setState({
    //       reportList:res.data
    //     })
    //   }
    // })
  }

  renderList = () => {
    const {reportList} = this.state
    return (
      reportList.map(t => {
        return (
          <div key={t.id}>
            <WhiteSpace size="lg" />
            <Card full>
              <Card.Header
                title={t.kcmc}
                extra={<span style={{fontSize:'0.9em'}}>{t.xn}年{t.xq}</span>}
              />
              <Card.Body>
                <div style={{marginTop:8,fontWeight:800}}>分数:{t.fs}</div>
              </Card.Body>
            </Card>
          </div>
        )
      })
    )
  }


  render () {
    return (
      <div>
        <NavBar
          mode="light"
          icon={<Icon type="left" />}
          onLeftClick={() => {router.go(-1)}}
          style={{
            borderBottom: '0.5px solid #eee'
          }}
        >我的成绩</NavBar>
        {this.renderList()}
      </div>
    )
  }
}

export default SchoolReport
