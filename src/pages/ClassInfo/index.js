import React from 'react'
import SelectClassPopuper from '@/components/SelectClassPopuper'
import { Icon, NavBar, WhiteSpace, Card } from 'antd-mobile';
import router from 'umi/router';
import http from '@/utils/http'

class ClassInfo extends React.Component{

  state = {
    open: false,
    classInfo:{},
    studentList: [
    ]
  }

  componentDidMount () {
    const {location:{query:{bjdm, bjmc}}} = this.props
    console.log(bjdm)
    const opt = {
      value:bjdm,
      children:bjmc
    }
    bjdm && this.popupChange(opt)
  }

  popupChange = opt => {
    this.setState({
      classInfo:opt
    })
    http(`/api/bus/myxsxx?bjdm=${opt.value}`).then(res => {
      console.log(res)
      this.setState({
        studentList:res.data
      })

    })
  }

  renderStudentList = () => {
    const {studentList} = this.state
    return  studentList.length > 0 ? (
      studentList.map(t => {
        return (
          <div key={t.zmr}>
            <WhiteSpace size="sm" />
            <Card full>
              <Card.Header
                title={t.xm}
                extra={<span style={{fontSize:'0.8em'}}>{t.xb}</span>}
              />
              <Card.Body>
                <div>联系电话:{t.lxdh}</div>
              </Card.Body>
              <Card.Footer content={t.xh} />
            </Card>
            <WhiteSpace size="sm" />
          </div>
        )
      })
    ) : (
      <div style={{width:'100%', height: '30%', textAlign: 'center',fontSize:'14px',color:'#222',marginTop:40,}}>暂无信息,请点击右上角选择班级</div>
    )
  }

  render () {
    const {classInfo, studentList} = this.state
    return (
     <div style={{height:'100%',}}>
       <NavBar
         mode="light"
         icon={<Icon type="left" />}
         onLeftClick={() => {router.go(-1)}}
         style={{
           borderBottom: '0.5px solid #eee'
         }}
         rightContent={[
           (<SelectClassPopuper onChange={this.popupChange}/>)
         ]}
       >我的班级</NavBar>
       <div style={{width:'100%',height:30,backgroundColor:'#fff',borderBottom:'1px solid #eee',textAlign:'center',lineHeight:'30px'}}>{classInfo.children}({studentList.length}人)</div>
       {this.renderStudentList()}
     </div>
    )
  }


}

export default ClassInfo
