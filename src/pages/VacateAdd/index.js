import React from 'react'
import { createForm, formShape } from 'rc-form';
import { Icon, NavBar, List, InputItem, WingBlank, Button, Toast, DatePicker, TextareaItem, Modal } from 'antd-mobile';
import router from 'umi/router';
import moment from 'moment'
import BaseSelect from '@/components/BaseSelect'
import http from '@/utils/http'
import SelectTypeCode from '@/components/SelectTypeCode';

const alert = Modal.alert;
const Item = List.Item;

class VacateAdd extends React.Component{

  static propTypes = {
    form: formShape,
  };

  state = {
    ksrq:null,
    jsrq:null,
    qjlb:null,
    qjlbDesc: null
  }

  postData = obj => {
    http.post('/api/bus/qjsq',{...obj}).then(res => {
      if(res.data.code === '0') {
        Toast.success('保存成功', 1);
        router.replace({
          pathname:'/vacate'
        })
      }
    })
  }


  onSubmit = () => {
    const {ksrq, jsrq, qjlb} = this.state
    alert('提示', '提交后不可更改,确认提交吗?', [
      { text: '取消', onPress: () => {} },
      { text: '确认', onPress: () => {
          this.props.form.validateFields({ force: true }, (error) => {
            if (!error) {
              const t = this.props.form.getFieldsValue()
              if (ksrq && jsrq && qjlb) {
                t.ksrq = ksrq
                t.jsrq = jsrq
                t.qjlb = qjlb
                this.postData(t)
              } else {
                Toast.fail('请检查是否有必填项未填!');
              }
            } else {
              Toast.fail('请检查是否有必填项未填!');
            }
          });
        }},
    ])


  }

  qjlbChange = val => {
    this.setState({
      qjlbDesc:val.codeDesc,
      qjlb:val.code,
    })
  }

  fromDateChange = date => {
    const {jsrq} = this.state
    const daysCount = moment(jsrq) - moment(date)
    console.log(daysCount)
    this.setState({ ksrq:moment(date).format('YYYY-MM-DD') })
  }

  endDateChange = date => {
    const {ksrq} = this.state
    this.setState({ jsrq:moment(date).format('YYYY-MM-DD')  })
  }

  render () {
    let errors;
    const { getFieldProps, getFieldError } = this.props.form;
    const {ksrq, jsrq, qjlbDesc} = this.state
    return (
      <div>
        <NavBar
          mode="light"
          icon={<Icon type="left" />}
          onLeftClick={() => {router.go(-1)}}
          style={{
            borderBottom: '0.5px solid #eee'
          }}
        >新建请假</NavBar>
        <form>
          <List>
            <DatePicker
              mode="date"
              title="Select Date"
              extra={ksrq}
              onChange={this.fromDateChange}
            >
              <List.Item arrow="horizontal"><span>起始日期<span style={{color:'red'}}>*</span></span></List.Item>
            </DatePicker>
            <DatePicker
              mode="date"
              title="Select Date"
              extra={jsrq}
              onChange={this.endDateChange}
            >
              <List.Item arrow="horizontal"><span>结束日期<span style={{color:'red'}}>*</span></span></List.Item>
            </DatePicker>
            <BaseSelect value={qjlbDesc} onChage={this.qjlbChange} label={<span>请假类型<span style={{color:'red'}}>*</span></span>} typeCode={SelectTypeCode.qingjia} />
            <InputItem
              {...getFieldProps('qjts', {
                // initialValue: 'little ant',
                rules: [
                  { required: true, message: '请输入请假天数' },
                ],
              })}
              clear
              error={!!getFieldError('qjts')}
              onErrorClick={() => {
                alert(getFieldError('qjts').join('、'));
              }}
              placeholder="请输入请假天数"
            ><span>请假天数<span style={{color:'red'}}>*</span></span></InputItem>
            <TextareaItem
              {...getFieldProps('qjyy', {
                // initialValue: 'little ant',
                rules: [
                  { required: true, message: '请输入请假事由' },
                ],
              })}
              clear
              error={!!getFieldError('qjyy')}
              onErrorClick={() => {
                alert(getFieldError('qjyy').join('、'));
              }}
              title={<span>请假事由<span style={{color:'red'}}>*</span></span>}
              placeholder="请输入请假事由"
              data-seed="logId"
              autoHeight
            />
          </List>
        </form>
        <WingBlank>
          <Button type="primary"  style={{marginTop:20}}  onClick={this.onSubmit}>保存</Button>
        </WingBlank>
      </div>
    )
  }

}

export default createForm()(VacateAdd);
