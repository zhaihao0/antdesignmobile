import React from 'react'
import http from '@/utils/http'
import { Icon, NavBar } from 'antd-mobile';
import router from 'umi/router'
import styles from './index.less'

class NewsView extends React.Component{

  state = {
    datas: {}
  }

  componentDidMount () {
    const {query:{id}} = this.props.location
    http(`/api/mlogin/noticeByid?id=${id}`).then(res => {
      console.log(res)
      if(res.data){
        this.setState({
          datas: res.data
        })
      }
    })
  }

  render () {
    const {datas} = this.state
    return (
      <>
        <NavBar
          mode="light"
          icon={<Icon type="left" />}
          onLeftClick={() => {router.go(-1)}}
          style={{
            borderBottom: '0.5px solid #eee'
          }}
        >新闻公告</NavBar>
        <div className={styles.newsContent} dangerouslySetInnerHTML={{__html: datas.content}}></div>
      </>

    )
  }

}

export default NewsView
