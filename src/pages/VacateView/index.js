import React from 'react'
import CommonFlowDisplay from '@/components/CommonFlowDisplay'
import StepsInfo from '@/components/StepsInfo'
import { Icon, NavBar } from 'antd-mobile';
import router from 'umi/router';

class VacateView extends React.Component{

  render () {
    const { id } = this.props.location.query
    return (
      <div>
        <NavBar
          mode="light"
          icon={<Icon type="left" />}
          onLeftClick={() => {router.go(-1)}}
          style={{
            borderBottom: '0.5px solid #eee'
          }}
        >请假详情</NavBar>
        <CommonFlowDisplay url={`/api/bus/spxx?id=${id}`} />
        <StepsInfo url={`/api/bus/dbmx?id=${id}`} />
      </div>
    )
  }

}

export default VacateView
