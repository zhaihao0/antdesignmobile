import React from 'react'
import http from '@/utils/http';
import {NavBar, Icon,  Card, WhiteSpace, Modal, Toast, Pagination, WingBlank  } from 'antd-mobile';
import router from 'umi/router';
import {stringify} from 'qs'
import styles from './index.less'

const alert = Modal.alert;

class Vacate extends React.Component{

  state = {
    vacateList: [],
    count: 0,
    pageIndex:1,
    upPageAble: false,
    downPageAble: true
  }


  componentDidMount () {
    const {pageIndex} = this.state
    this.fetchData(pageIndex)
  }

  fetchData = (index) => {
    http.post(`/api/bus/xsqjsq?${stringify({pageIndex:index,pageSize:8})}`).then(res => {
      if(res.data.data) {
        this.setState({
          vacateList: res.data.data,
          count:Math.ceil(res.data.count / 8)
        })
      }
    })
  }


  add = () => {
    router.push(
      {
        pathname:'/vacateAdd',
      }
    )
  }

  pageUper = () => {
    const {pageIndex} = this.state
    let index = 0
    if(pageIndex > 1) {
      index = pageIndex -1
      this.setState({
        pageIndex:index
      }, () => {
        this.fetchData(index)
      })
    } else {
      this.setState({
        upPageAble:false
      })
    }

  }

  pageDowner = () => {
    const {pageIndex} = this.state
    let index = 0
    index = pageIndex + 1
    this.setState({
      pageIndex:index
    }, () => {
      this.fetchData(index)
    })
  }

  cardActions = (obj) => {
    const inStyles = {
      fontSize:12,
      marginLeft:15,
    }
    return (
      <>
        <span className={styles.buttons} style={{color:'red', ...inStyles}} onClick={(e) => {this.del(e,obj)}}>删除</span>
        <span className={styles.buttons} style={{color:'#168ee9', ...inStyles}} onClick={(e) => {this.edit(e,obj)}} >编辑</span>
      </>
    )
  }

  view = id =>{
    router.push({
      pathname:'/vacateView',
      query:{
        id
      }
    })
  }

  renderList = () => {
    const {vacateList} = this.state
    return (
      vacateList.map(t => {
        let state = null
        let color = '#ccc'
        if(t.lczt  === '1') {
          state = '审批中'
          color = '#449dea'
        } else if(t.lczt === '2') {
          state = '通过'
          color = 'green'
        } else if(t.lczt === '4') {
          state = '未通过'
          color = 'red'
        }
        return (
          <div key={t.id}>
            <WhiteSpace size="lg" />
            <Card full onClick={() => {this.view(t.id)}}>
              <Card.Header
                title={<span>{t.qjlb} <span style={{fontSize:10,color,}}> {state} </span></span>}
                extra={<span style={{fontSize:12,color:'red'}}>共{t.qjts}天</span>}
              />
              <Card.Body>
                <div>请假事由: {t.qjyy}</div>
              </Card.Body>
              <Card.Footer content={`开始:${t.ksrq}`} extra={`结束:${t.jsrq}`} />
            </Card>
          </div>
        )
      })
    )
  }


  render () {
    const { count, pageIndex } = this.state
    return (
      <div>
        <NavBar
          mode="light"
          icon={<Icon type="left" />}
          onLeftClick={() => {router.go(-1)}}
          style={{
            borderBottom: '0.5px solid #eee'
          }}
          rightContent={[
            <span key={0} onClick={() => {this.add()}} style={{fontSize:14}}>新建请假</span>,
          ]}
        >请假信息</NavBar>
        {this.renderList()}
        <WingBlank>
          <Pagination
            total={count}
            current={pageIndex}
            simple
            style={{marginTop:20,marginBottom:20}}
            locale={{
            prevText: (<span style={{fontSize:13}} onClick={this.pageUper}>上一页</span>),
            nextText: (<span style={{fontSize:13}} onClick={this.pageDowner}>下一页</span>),
          }}
          />
        </WingBlank>
      </div>
    )
  }

}

export default Vacate
