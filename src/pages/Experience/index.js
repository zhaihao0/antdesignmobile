// 个人经历
import React from 'react'
import router from 'umi/router'
import { NavBar, Icon, Card, WhiteSpace, Toast, Modal } from 'antd-mobile';
import http from '@/utils/http'
import {stringify} from 'qs'
import styles from '@/pages/Family/index.less';

const alert = Modal.alert;

class Experience extends React.Component{

  state = {
    ExperienceList: []
  }


  componentDidMount () {
    this.fetchData()
  }

  fetchData = () => {
    http(`/api/mlogin/xsGrjl`).then(res => {
      if(res.data){
        this.setState({
          ExperienceList:res.data
        })
      }
    })
  }


  del = (e,obj) =>{
    e.stopPropagation()
    alert('删除', '确认删除吗?', [
      { text: '取消', onPress: () => {} },
      { text: '确认', onPress: () => {
          http(`/api/bus/delJl?${stringify({id:obj.id})}`).then(res => {
            // Todo 确认是否是Get请求 现在这种方式会报 500
            if(res.data.code === '0') {
              Toast.success('删除成功')
              this.fetchData()
            }
          })
        }},
    ])
  }

  edit = (e,obj) => {
    e.stopPropagation()
    router.push(
      {
        pathname:'/experienceEdit',
        query:obj
      }
    )
  }

  add = () => {
    router.push(
      {
        pathname:'/experienceAdd',
      }
    )
  }

  cardActions = (obj) => {
    const inStyles = {
      fontSize:12,
      marginLeft:15,
    }
    return (
      <>
        <span className={styles.buttons} style={{color:'red', ...inStyles}} onClick={(e) => {this.del(e,obj)}}>删除</span>
        <span className={styles.buttons} style={{color:'#168ee9', ...inStyles}} onClick={(e) => {this.edit(e,obj)}} >编辑</span>
      </>
    )
  }


  renderList = () => {
    const {ExperienceList} = this.state
    return (
      ExperienceList.map(t => {
        return (
          <div key={t.zmr}>
            <WhiteSpace size="lg" />
            <Card full>
              <Card.Header
                title={t.qsrq}
                extra={this.cardActions(t)}
              />
              <Card.Body>
                <div>{t.hdgzxx}</div>
                <div>证明人:{t.zmr}</div>
              </Card.Body>
              <Card.Footer content={t.qsrq} extra={t.jsrq} />
            </Card>
          </div>
        )
      })
    )
  }


  render () {
    return (
      <div>
        <NavBar
          mode="light"
          icon={<Icon type="left" />}
          onLeftClick={() => {router.go(-1)}}
          style={{
            borderBottom: '0.5px solid #eee'
          }}
          rightContent={[
            <span key={0} onClick={() => {this.add()}} style={{fontSize:14}}>添加</span>,
          ]}
        >个人经历</NavBar>
        {this.renderList()}
      </div>
    )
  }
}

export default Experience
