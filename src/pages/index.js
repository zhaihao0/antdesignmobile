import React from 'react'
import BaseTabBar from '../components/BaseTabBar'
import Me from './Me'
import MenuHome from './MenuHome'
import MessagePage from './Message'
import messageIcon from '../assets/icon/icon_message.svg'
import messageIconFill from '../assets/icon/icon_message_fill.svg'
import homeIcoon from '../assets/icon/icon_work.svg'
import homeIconFill from '../assets/icon/icon_work_fill.svg'
import meIcon from '../assets/icon/icon_signal.svg'
import meIconFill from '../assets/icon/icon_signal_fill.svg'
import router from 'umi/router';
import { Button,WingBlank } from 'antd-mobile';
// import ImagePickerCom from '../components/imgUpload'
// import FloatingSearch from '../components/FloatingSearch'
import http from '@/utils/http';
// import { stringify } from 'qs';
// import Styles from './Me/index.js.less'
// import BaseBottomTabs from '../components/BaseBottomTabs'

class Index extends React.Component{

  state = {
    userInfo:{},
    page: ''
  }

  componentDidMount() {
    this.getUserInfo()
    // alert(window.localStorage.getItem('currentpage'))
    this.setState({
      page:window.localStorage.getItem('currentpage')
    })
  }

  getUserInfo = () => {
    http(`/api/mlogin/mUserInfo`).then(Res => {
      console.log(Res.data)
      if(Res.data) {
        this.setState({
          userInfo:Res.data
        })
      }
    })
  }

  goLogin = () => {
    router.replace('/login')
  }

  pageChange = val => {
    window.localStorage.setItem('currentpage',val)
    this.setState({
      page:val
    })
  }

  render () {
    const {userInfo, page} = this.state
    let pages = {
      'message': (
        <MessagePage />
      ),
      'home': (
        <MenuHome />
      ),
      'me': (
        <Me userInfo={userInfo} />
      )
    }
    let tabBarLists = [
      {
        title:'待办',
        key:'message',
        badge:'',
        icon: messageIcon,
        selectedIcon: messageIconFill,
      },
      {
        title:'主页',
        key:'home',
        badge:'',
        icon: homeIcoon,
        selectedIcon: homeIconFill,
      },
      {
        title:'我的',
        key:'me',
        badge:'',
        icon: meIcon,
        selectedIcon: meIconFill,
      }
    ]
    return (
      <div style={{
        width:'100%',
        height:'100%',
        position:'fixed',
        bottom: 0,
        backgroundColor:'#fff'
      }}>
        <BaseTabBar pages={pages} onChange={this.pageChange} selectedTab={page} items={tabBarLists} />
      </div>
    )
  }
}

export default Index
