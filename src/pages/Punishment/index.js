import React from 'react'
import http from '@/utils/http';
import {NavBar, Icon,  Card, WhiteSpace   } from 'antd-mobile';
import router from 'umi/router';

class Punishment extends React.Component{


  state = {
    PunishmentList: []
  }


  componentDidMount () {
    http(`/api/mlogin/xsCfxx`).then(res => {
      console.log(res.data)
      if(res.data){
        this.setState({
          PunishmentList:res.data
        })
      }
    })
  }


  renderList = () => {
    const {PunishmentList} = this.state
    return (
      PunishmentList.map(t => {
        return (
          <div key={t.wh}>
            <WhiteSpace size="lg" />
            <Card full>
              <Card.Header
                title={t.cflx}
                extra={t.jldj}
              />
              <Card.Body>
                <div>{t.cfyy}-{t.bz}</div>
              </Card.Body>
              <Card.Footer content={t.wh} extra={t.cfrq} />
            </Card>
          </div>
        )
      })
    )
  }


  render () {
    return (
      <div>
        <NavBar
          mode="light"
          icon={<Icon type="left" />}
          onLeftClick={() => {router.go(-1)}}
          style={{
            borderBottom: '0.5px solid #eee'
          }}
        >处分信息</NavBar>
        {this.renderList()}
      </div>
    )
  }

}

export default Punishment
