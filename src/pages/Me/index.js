import React from 'react'
import Styles from './index.less'
import { Flex, List } from 'antd-mobile'
import router from 'umi/router';
import http from '@/utils/http'
import picture1 from '../../assets/p1.png'
import grinfo from '../../assets/grinfo.png'
import bjinfo from '../../assets/bjinfo.png'
import jtinfo from '../../assets/jt.png'
import jlinfo from '../../assets/jl.png'
import cfinfo from '../../assets/cf.png'
import jianglinfo from '../../assets/jiangli.png'
import settings from '../../assets/setting.png'

const {Item} = List

class Me extends React.Component{

  state = {
    classList:[]
  }

  componentDidMount () {
    const {userInfo} = this.props
    http('/api/bus/mybjxx').then(res => {
      this.setState({
        classList:res.data
      })
    })
    window.localStorage.setItem('currentpage','me')

  }

  go = (path,obj) => {
    if(obj !== undefined) {
      router.push({
        pathname:path,
        query:{
          userInfo: obj
        }
      })
    } else {
      router.push({
        pathname:path,
      })
    }
  }

  loginOut = () => {
    http('/api/mlogin/logout').then(res => {
      if (res.data.code === '0') {
        router.replace({
          pathname:'/login'
        })
      }
    })
  }

  studentInfo = () => {
    const {userInfo} = this.props
    return (
      <div className={Styles.wraps}>
        <div className={Styles.tops}>
          <div className={Styles.names}>
            <div className={Styles.picture}>
              <img src={picture1} alt=""/>
            </div>
            <div className={Styles.name}>
              <div className={Styles.top}>{userInfo.xm || ''} <span style={{fontSize:'0.6em',fontWeight: 100}}>学生</span></div>
              <div className={Styles.bottom}>{userInfo.dwmc}{userInfo.bjmc}</div>
            </div>
          </div>
        </div>
        <div className={Styles.floatCardsA}>
          <div className={Styles.funs}>
            <Flex style={{height:'100%'}}>
              <Flex.Item>
                <div className={Styles.funCon} onClick={() => {this.go('/personage', userInfo)}}>
                  <div className={Styles.iconTop}>
                    <img src={grinfo} alt=""/>
                  </div>
                  <div className={Styles.nameBottom}>
                    个人信息
                  </div>
                </div>
              </Flex.Item>
              {/*<Flex.Item>*/}
                {/*<div className={Styles.funCon}>*/}
                  {/*<div*/}
                    {/*className={Styles.iconTop}*/}
                  {/*>*/}
                    {/*<img src={bjinfo} alt=""/>*/}
                  {/*</div>*/}
                  {/*<div className={Styles.nameBottom}>*/}
                    {/*班级信息*/}
                  {/*</div>*/}
                {/*</div>*/}
              {/*</Flex.Item>*/}
            </Flex>
          </div>
        </div>

        <div className={Styles.floatCardsB}>
          <div className={Styles.funs}>
            <Flex style={{height:'100%'}}>
              <Flex.Item>
                <div className={Styles.funCon} onClick={() => {this.go('/family')}}>
                  <div className={Styles.iconTop}>
                    <img src={jtinfo} alt=""/>
                  </div>
                  <div className={Styles.nameBottom}>
                    家庭信息
                  </div>
                </div>
              </Flex.Item>
              <Flex.Item>
                <div className={Styles.funCon} onClick={() => {this.go('/experience')}}>
                  <div
                    className={Styles.iconTop}
                  >
                    <img src={jlinfo} alt=""/>
                  </div>
                  <div className={Styles.nameBottom}>
                    活动经历
                  </div>
                </div>
              </Flex.Item>
            </Flex>
          </div>
        </div>

        <div className={Styles.floatCardsC}>
          <div className={Styles.funs}>
            <Flex style={{height:'100%'}}>
              <Flex.Item>
                <div className={Styles.funCon} onClick={() => {this.go('/award')}}>
                  <div className={Styles.iconTop}>
                    <img src={jianglinfo} alt=""/>
                  </div>
                  <div className={Styles.nameBottom}>
                    我的奖励
                  </div>
                </div>
              </Flex.Item>
              <Flex.Item>
                <div className={Styles.funCon} onClick={() => {this.go('/punishment')}}>
                  <div
                    className={Styles.iconTop}
                  >
                    <img src={cfinfo} alt=""/>
                  </div>
                  <div className={Styles.nameBottom}>
                    我的处分
                  </div>
                </div>
              </Flex.Item>
            </Flex>
          </div>
        </div>
        <div className={Styles.settings} onClick={this.loginOut}>
          <img src={settings} alt="" />退出登录
        </div>

      </div>
    )
  }

  goClassInfo = (bjdm,bjmc) => {
    router.push({
      pathname:'/classInfo',
      query:{
        bjdm,
        bjmc
      }
    })
  }

  teacherInfo = () => {
    const {userInfo} = this.props
    const {classList} =this.state
    return (
      <div className={Styles.wraps}>
        <div className={Styles.tops}>
          <div className={Styles.names}>
            <div className={Styles.picture}>
              <img src={picture1} alt=""/>
            </div>
            <div className={Styles.name}>
              <div className={Styles.top}>{userInfo.jsxm || ''} <span style={{fontSize:'0.6em',fontWeight: 100}}>教职工</span></div>
              <div className={Styles.bottom}>{userInfo.dwmc}{userInfo.bjmc}</div>
            </div>
          </div>
        </div>

        <div className={Styles.teacherCard}>
          <List renderHeader={() => '带班信息'}>
          {classList.length > 0 ? classList.map(t => {
            return (
              <Item key={t.id} onClick={() => {this.goClassInfo(t.id, t.bjmc)}}>{t.bjmc}</Item>
            )
          }) : ( <Item>暂无带班信息</Item>)}
          </List>
          <div className={Styles.tsettings} onClick={this.loginOut}>
            <img src={settings} alt="" />退出登录
          </div>
        </div>



      </div>
    )
  }

  wrap = () => {
    const {userInfo} = this.props
    return userInfo.userType === '1' ? this.teacherInfo() : this.studentInfo()
   }

  render () {
    return this.wrap()
  }
}

export default Me



