import React from 'react'
import { WingBlank, WhiteSpace } from 'antd-mobile';
import MessageList from '@/components/MessageList'
import styles from './index.less'

class MessagePage extends React.Component{

  componentDidMount () {
    window.localStorage.setItem('currentpage','message')
  }
  render () {
    return (
      <>
        <div className={styles.wraps}>
          <WingBlank>
            <MessageList />
          </WingBlank>
        </div>
      </>
    )
  }
}

export default MessagePage
