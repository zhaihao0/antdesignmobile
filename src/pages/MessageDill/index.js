import React from 'react'
import CommonFlowDisplay from '@/components/CommonFlowDisplay'
import StepsInfo from '@/components/StepsInfo'
import { Icon, NavBar, Modal, Flex, Button, WingBlank, Toast } from 'antd-mobile';
import router from 'umi/router';
import http from '@/utils/http'
import {stringify} from 'qs'

const prompt = Modal.prompt;

class MessageDill extends React.Component{

  butAction = actionType => {
    if(actionType === 'notgo') {
      prompt('请输入审批不通过意见', '',
        [
          {
            text: '取消',
            onPress: () => {}
          },
          {
            text: '提交',
            onPress:value => {this.submitData(actionType, value)}
          },
        ], 'default', '不通过', ['请输入审批不通过意见']
      )

    } else if (actionType === 'adopt') {
      prompt('请输入审批通过意见', '',
        [
          {
            text: '取消',
            onPress: () => {}
          },
          {
            text: '提交',
            onPress:value => {this.submitData(actionType, value)}
          },
        ], 'default', '通过', ['请输入审批通过意见']
      )

    }
  }

  submitData = (RESULT,message) => {
    const {taskId} = this.props.location.query
    const parmas = {
      RESULT,
      message,
      taskId
    }
    http(`/api/bus/handle?${stringify(parmas)}`).then(res => {
      if (res.data.code === '0') {
        Toast.success('操作成功')
        router.push({
          pathname:'/'
        })
      } else {
        Toast.info(res.data.msg)
      }
    })
  }

  render () {
    const { id } = this.props.location.query
    return (
      <div>
        <NavBar
          mode="light"
          icon={<Icon type="left" />}
          onLeftClick={() => {router.go(-1)}}
          style={{
            borderBottom: '0.5px solid #eee'
          }}
        >待办详细</NavBar>
        <CommonFlowDisplay url={`/api/bus/spck?proccessId=${id}`} />
        <StepsInfo url={`/api/bus/spckmx?proccessId=${id}`} />
        <div style={{marginTop:20}}>
          <WingBlank>
            <Flex>
              <Flex.Item>  <Button type="warning" onClick={() => {this.butAction('notgo')}}>不通过</Button></Flex.Item>
              <Flex.Item>  <Button type="primary" onClick={() => {this.butAction('adopt')}}>通过</Button></Flex.Item>
            </Flex>
          </WingBlank>
        </div>

      </div>
    )
  }

}

export default MessageDill
