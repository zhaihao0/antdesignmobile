import React from 'react'

class ClassMute extends React.Component{

  render () {
    return (
      <div style={{width:'100%',height:'100px',textAlign:'center',fontSize:'14px',color:'#ccc', lineHeight:'100px'}}>暂无课表信息</div>
    )
  }

}

export default ClassMute
