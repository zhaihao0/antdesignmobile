import React from 'react'
import SelectClassPopuper from '@/components/SelectClassPopuper'
import { Icon, NavBar, WhiteSpace, Card } from 'antd-mobile';
import router from 'umi/router';
import http from '@/utils/http'
import styles from './index.less'

class Attendance extends React.Component{

  state = {
    open: false,
    classInfo:{},
    studentList: [
      {
        xh:'001',
        lxdh:'13111223451',
        xm: '张三',
        xb:'男',
        id:'01',
        came:false
      },
      {
        xh:'002',
        lxdh:'13111223451',
        xm: '李四',
        xb:'男',
        id:'02',
        came:true
      },
      {
        xh:'003',
        lxdh:'13331223451',
        xm: '王五',
        xb:'男',
        id:'03',
        came:true
      },{
        xh:'004',
        lxdh:'13145223451',
        xm: '赵柳',
        xb:'男',
        id:'04',
        came:false
      },
      {
        xh:'005',
        lxdh:'13116723451',
        xm: '张三丝',
        xb:'男',
        id:'05',
        came:false
      },

    ]
  }

  popupChange = opt => {
    this.setState({
      classInfo:opt
    })
    http(`/api/bus/mybjxx?bjdm=${opt.value}`).then(res => {
      console.log(res)
      // Todo 后期需要调整接口返回数据 现在是假数据

    })
  }

  toggleState = (xh,index) => {
    const {studentList} = this.state
    let ts = studentList
    ts[index].came = !ts[index].came
    this.setState({
      studentList: ts
    })
  }

  renderStudentList = () => {
    const {studentList} = this.state
    return  (
      studentList.map((t, index) => {
        return (
          <div key={t.xh}>
            <WhiteSpace size="sm" />
            <Card full onClick={() => {this.toggleState(t.xh, index)}}>
              <Card.Header
                title={t.xm}
                extra={<span style={{fontSize:'0.8em'}}>{t.xb}</span>}
              />
              <Card.Body>
                <div>学号:{t.xh}</div>
                <div style={{marginTop:8}}>联系电话:{t.lxdh}</div>
              </Card.Body>
              <Card.Footer extra={t.came ? (<span style={{color:'green'}}>已签到</span>): (<span style={{color:'red'}}>未签到</span>)} />
            </Card>
            <WhiteSpace size="sm" />
          </div>
        )
      })
    )
  }

  render () {
    const {classInfo} = this.state
    return (
      <div>
        <NavBar
          mode="light"
          icon={<Icon type="left" />}
          onLeftClick={() => {router.go(-1)}}
          style={{
            borderBottom: '0.5px solid #eee'
          }}
          rightContent={[
            (<span style={{fontSize:'0.8em'}}> 提交 </span>),
            (<SelectClassPopuper style={{fontSize:'0.8em',marginLeft:15}} onChange={this.popupChange}/>),
          ]}
        >我的班级</NavBar>
        <div style={{width:'100%',height:30,backgroundColor:'#fff',borderBottom:'1px solid #eee',textAlign:'center',lineHeight:'30px'}}>{classInfo.children}</div>
        {this.renderStudentList()}
      </div>
    )
  }


}

export default Attendance
