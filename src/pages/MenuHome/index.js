import React from 'react'
import {
  Carousel, WingBlank, WhiteSpace, Card } from 'antd-mobile';
import MainTitlle from '@/components/MainTitle'
import GridMenu from '@/components/GridMenu'
import http from '@/utils/http'
import moment from 'moment'
import router from 'umi/router'
import {stringify} from 'qs'
import Styles from './index.less'
import banner from '../../assets/banner.png'
import banner1 from '../../assets/banner1.png'

class MenuHome extends React.Component{

  state = {
    bannerData: [{id:0,img:banner}, {id:1,img:banner1}],
    newsList: [],
    pager:{
      pageIndex:1,
      pageSize:5
    },
    allShow:false
  }

  componentDidMount () {
    const {pager} = this.state
    window.localStorage.setItem('currentpage','home')
    http(`/api/mlogin/notice?${stringify({...pager})}`,).then(res => {
      if(res.data) {
        if(res.data.data.length > 0) {
          let bannerData = res.data.data.filter(t => {
            return t.isTop === '1'
          })
          this.setState({
            bannerData,
          })
          this.setState({
            newsList:res.data.data,
          })
        } else {
          this.setState({
            allShow:true,
          })
        }
      }
    })
  }

  fecthData = () => {
    const {pager ,newsList} = this.state
    const {pageIndex} = pager
    const pageIndexs = pageIndex + 1
    this.setState({
      pager:{
        ...pager,
        pageIndex:pageIndexs
      }
    }, () => {
      http(`/api/mlogin/notice?${stringify({pageIndex:pageIndexs,pageSize:5})}`,).then(res => {
        if(res.data) {
          if(res.data.data.length > 0) {
            const newsLists = [...newsList, ...res.data.data]
            this.setState({
              newsList:newsLists,
            })
          } else {
            this.setState({
              allShow:true,
            })
          }
        }
      })
    })
  }


  headerBanner = () => {
    const {bannerData} = this.state
    const borderStyle = {
      borderRadius:8,
      overflow:'hidden',
    }
    return (
      <WingBlank>
        <div className={Styles.CarouselWrap}>
          <Carousel
            autoplay={false}
            infinite
            beforeChange={(from, to) => console.log(`slide from ${from} to ${to}`)}
            afterChange={index => console.log('slide to', index)}
            style={{top:20}}
          >
            {bannerData.map(t => (
              <a
                key={t.id}
                style={{ display: 'inline-block', width: '100%', ...borderStyle }}
              >
                <img
                  src={t.img}
                  alt={t.id}
                  style={{ width: '100%', height: 150, verticalAlign: 'top' }}
                  onLoad={() => {
                    // fire window resize event to change height
                    window.dispatchEvent(new Event('resize'));
                    this.setState({ imgHeight: 'auto' });
                  }}
                />
              </a>
            ))}
          </Carousel>
        </div>
      </WingBlank>
    )
  }

  menusGrids = () => {
    return (
      <>
        <WhiteSpace size="xl" />
          <MainTitlle title='校园服务' />
          <GridMenu />
      </>
    )
  }

  viewNews = id => {
    router.push({
      pathname:'/newsView',
      query:{
        id:id
      }
    })
  }

  newsList = () => {
    const {newsList,allShow} = this.state
    return (
      <>
        <MainTitlle title='新闻公告' />
        <div className={Styles.cardList} style={{backgroundColor:'#fff',marginTop:20}}>
          {newsList.map(t => {
            return (
              <WingBlank size="lg" key={t.id}>
                <WhiteSpace size="sm" />
                <Card onClick={() => {this.viewNews(t.id)}}>
                  <Card.Header
                    title={<span style={{fontSize:'0.8em',fontWeight:800}}>{t.title.length >= 12 ? `${t.title.substring(0,10)}...` : t.title}</span>}
                    extra={<span style={{fontSize:'0.7em'}}>{t.stype === '1' ? '公告' : '新闻'}</span>}
                  />
                  <Card.Body>
                    <div dangerouslySetInnerHTML={{__html: t.tabloid}}></div>
                  </Card.Body>
                  <Card.Footer content={<span style={{fontSize:'0.7em'}}>{moment(t.lastUpdTime).format('YYYY-MM-DD')}</span>} extra={t.lastUpdAcct} />
                </Card>
                <WhiteSpace size="sm" />
              </WingBlank>
            )
          })}
        </div>
        <WhiteSpace size="sm" />
        {allShow ? ( <div style={{width:'100%',height:30,color:'#333',textAlign:'center'}}>已加载全部</div>) : ( <div style={{width:'100%',height:30,color:'blue',textAlign:'center'}} onClick={this.fecthData}>加载更多</div>)}

      </>
    )
  }

  wrap = () => {
    return (
      <div className={Styles.wraps}>
        {this.headerBanner()}
        {this.menusGrids()}
        {this.newsList()}
      </div>
    )
  }


  render () {
    return this.wrap()
  }
}

export default MenuHome



