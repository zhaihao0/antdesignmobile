import React from 'react'
import router from 'umi/router'
import { NavBar, Icon,  Card, WhiteSpace  } from 'antd-mobile';
import http from '@/utils/http'

class Award extends React.Component{

  state = {
    awardList: []
  }


  componentDidMount () {
    http(`/api/mlogin/xsJlxx`).then(res => {
      console.log(res.data)
      if(res.data){
        this.setState({
          awardList:res.data
        })
      }
    })
  }

  renderList = () => {
    const {awardList} = this.state
    return (
      awardList.map(t => {
        return (
          <div key={t.wh}>
            <WhiteSpace size="lg" />
            <Card full>
              <Card.Header
                title={t.jlrr}
                extra={t.jldj}
              />
              <Card.Body>
                <div>{t.jldj}-{t.bz}</div>
              </Card.Body>
              <Card.Footer content={t.wh} extra={t.jlsj} />
            </Card>
          </div>
        )
      })
    )
  }


  render () {
    return (
      <div>
        <NavBar
          mode="light"
          icon={<Icon type="left" />}
          onLeftClick={() => {router.go(-1)}}
          style={{
            borderBottom: '0.5px solid #eee'
          }}
        >奖励信息</NavBar>
        {this.renderList()}
      </div>
    )
  }
}

export default Award
