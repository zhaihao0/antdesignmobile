import React from 'react'
import { WingBlank, WhiteSpace, Grid } from 'antd-mobile';
import router from 'umi/router'
import http from '@/utils/http';
import Micon from '@/components/MenuIcon'
import Mr from '@/components/MenuRouter'

class GridMenu extends React.Component{
  state = {
    menuData:[
    ]
  }

  componentDidMount () {
    http(`/api/mlogin/menu`).then(Res => {
      if(Res.data) {
        console.log(Res.data)
        const menuData = Res.data.map(t => {
          let ts = t
          ts.icon = Micon[t.menuCode]
          ts.text = t.menuName
          return ts
        })
          this.setState({
           menuData,
        })
      }
    })
  }

  menuClick = (obj, index) => {
    router.push({
      pathname:Mr[obj.menuCode]
    })
  }

  render () {
    const {menuData} = this.state
    return (
      <>
        <div>
          <WhiteSpace size="sm" />
          <WingBlank>
            {menuData.length > 0 ? (<Grid data={menuData} hasLine={false} onClick={this.menuClick}/>) : (<div style={{width:'100%', textAlign:'center',color:'#ccc',height:'30px',lineHeight:'30px',fontSize:'12px'}} >暂无可用菜单</div>)}
          </WingBlank>
          <WhiteSpace size="lg" />
        </div>
      </>
    )
  }
}

export default GridMenu
