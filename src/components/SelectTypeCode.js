// 政治面貌:ZZMM_XX
// 家庭关系：JTXX_GX
// 请假类型: XS_QJLB

const selectTypeCode = {
  zhengzhimianmao:'ZZMM_XX',
  jiatingguanxi:'JTXX_GX',
  qingjia:'XS_QJLB',
  minzu:'MZDM_XX'
}

export default selectTypeCode
