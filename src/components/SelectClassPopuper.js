import React from 'react'
import { List,Popover, Icon } from 'antd-mobile';
import http from '@/utils/http'
import './SelectClassPopuper.less'

const Item = Popover.Item;

class SelectClassPopuper extends React.Component {
  state = {
    classList: [
    ]
  }

  componentDidMount () {
    const {onChange} =this.props
    http(`/api/bus/mybjxx`).then(res => {
      if(res.data) {
        this.setState({
          classList: res.data
        })
      }
    })
  }

  onSelect = (opt) => {
    const {onChange} =this.props
    // console.log(opt.props.value);
    onChange(opt.props)
    this.setState({
      visible: false,
      selected: opt.props.value,
    });
  };
  handleVisibleChange = (visible) => {
    this.setState({
      visible,
    });
  };

  render() {
    const {classList} = this.state
    const {open,selectedId} = this.props
    const sidebar = (
      <List>
        {classList.map((t, index) => {
          return (<List.Item key={t.id}><span style={{color:selectedId === t.id ? '#449dea' : ''}}>{t.bjmc}</span></List.Item>);
        })}
     </List>
    );
    return classList.length > 0 ? (
      <Popover mask
               overlayClassName="fortest"
               overlayStyle={{ color: 'currentColor' }}
               visible={this.state.visible}
               overlay={
                 classList.map(t => {
                   return (
                     <Item key={t.id}  value={t.id}  data-seed="logId">{t.bjmc}</Item>
                   )
                 })
               }
               align={{
                 overflow: { adjustY: 0, adjustX: 0 },
                 offset: [-10, 0],
               }}
               onVisibleChange={this.handleVisibleChange}
               onSelect={this.onSelect}
      >
        <div style={{
          height: '100%',
          padding: '0 15px',
          marginRight: '-15px',
          display: 'flex',
          alignItems: 'center',
        }}
        >
          <Icon type="ellipsis" />
        </div>
      </Popover>
    ) : null
  }
}


export default SelectClassPopuper
