import React from 'react'
import { Picker, List } from 'antd-mobile';
import http from '@/utils/http'

const CustomChildren = props => (
  <div
    onClick={props.onClick}
    style={{ backgroundColor: '#fff', paddingLeft: 15 }}
  >
    <div className="test" style={{ display: 'flex', height: '45px', lineHeight: '45px' }}>
      <div style={{ flex: 1, overflow: 'hidden', textOverflow: 'ellipsis', whiteSpace: 'nowrap' }}>{props.children}</div>
      <div style={{ textAlign: 'right', color: '#888', marginRight: 15 }}>{props.extra}</div>
    </div>
  </div>
);

class BaseSelect extends  React.Component{

  state = {
    optionList: []
  }

  componentDidMount () {
    const {typeCode} = this.props
    this.getSelectOptions(typeCode)
  }

  picker = () => {
    const {optionList} = this.state
    const {label, value} = this.props
    return (
      <Picker data={optionList}  onOk={this.onOk} extra={value} className="forss" cols={1}>
        <List.Item arrow="horizontal">{label}</List.Item>
      </Picker>
    )
  }

  onOk = val => {
    console.log(val)
    const { onChage } = this.props
    const {optionList} = this.state
    let obj = optionList.filter(t => {
      return t.code == val[0]
    })
    onChage(obj[0])
  }

  getSelectOptions = (typeCode) => {
    http(`/api/bus/typeCodeList?typecode=${typeCode}`).then(Res => {
      if(Res.data) {
        const optionList = Res.data.map(t => {
          let ts = t
          ts.value = t.code
          ts.label = t.codeDesc
          return ts
        })
          this.setState({
          optionList
        })
      }
    })
  }

  render () {
   return this.picker()
  }

}

export default BaseSelect
