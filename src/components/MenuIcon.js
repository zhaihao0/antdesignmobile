import qj from '@/assets/menuIcons/qj.png'
import bj from '@/assets/menuIcons/bj.png'
import kb from '@/assets/menuIcons/kb.png'
import cjd from '@/assets/menuIcons/cjd.png'
import qd from '@/assets/menuIcons/qd.png'

const Micon = {
  qj:qj,
  bj:bj,
  kb:kb,
  cjd:cjd,
  qd:qd
}

export default Micon
