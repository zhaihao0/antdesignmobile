// 图片压缩上传
import React from 'react'
import { ImagePicker, WingBlank, SegmentedControl } from 'antd-mobile';
import lrz from 'lrz'

class ImagePickerCom extends React.Component {
    state = {
        files: [],
        multiple: false,
      }

      onChange = (files, type, index) => {
        console.log(files[0], type, index);
        this.setState({
          files,
        });
        lrz(files[0].file,{quality :0.2})
        .then(function (rst) {
            // 处理成功会执行
            console.log(rst);
        })
        .catch(function (err) {
            // 处理失败会执行
            console.log(err);
            console.log('111www1222')
        })
        .always(function () {
            // 不管是成功失败，都会执行
            console.log('1111222')
        });
      }

      onSegChange = (e) => {
        const index = e.nativeEvent.selectedSegmentIndex;
        this.setState({
          multiple: index === 1,
        });
      }

      render() {
        const { files } = this.state;
        return (
          <WingBlank style={{borderBottom:'1px solid #eee'}}>
            <SegmentedControl
              values={['切换到单选1', '切换到多选2']}
              selectedIndex={this.state.multiple ? 1 : 0}
              onChange={this.onSegChange}
            />
            <ImagePicker
              files={files}
              onChange={this.onChange}
              onImageClick={(index, fs) => console.log(index, fs)}
              selectable={files.length < 7}
              multiple={this.state.multiple}
            />
          </WingBlank>
        );
      }
    
}


export default ImagePickerCom