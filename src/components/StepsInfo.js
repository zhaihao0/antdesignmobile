import React from 'react'
import { Steps,WhiteSpace, WingBlank, Icon } from 'antd-mobile';
import http from '@/utils/http'
import styles from './StepsInfo.less'

const Step = Steps.Step;

class StepsInfo extends React.Component{

  state = {
    dataList:[]
  }


  componentDidMount () {
    const {url} = this.props
    http(url).then(res => {
      if(res.data){
        this.setState({
          dataList:res.data
        })
      }
    })
  }

  render () {
    const {dataList} =this.state
    const points = {
      width:20,
      height:20,
      borderRadius:10,
      backgroundColor:'#449dea',
      color:'#fff !important',
      textAlign:'center',
      lineHeight:'20px',
      fontSize:'0.8em'
    }
    return (
      <div className={styles.StepsInfo} style={{backgroundColor:'#fff'}}>
        <WingBlank size="lg">
          <WhiteSpace size="lg"/>
          <Steps size="small">
            {dataList.reverse().map((t,index) => {
              return (
                <Step key={t.taskId} title={t.message} icon={<div style={{...points}}>{index + 1}</div>} description={`${t.taskName} ${t.assignee} ${t.endTime}`} />
              )
            })}
          </Steps>
        </WingBlank>
      </div>
    )
  }

}

export default StepsInfo
