import React from 'react'
import { List, WhiteSpace } from 'antd-mobile';
import http from '@/utils/http'
import {stringify} from 'qs'
import router from 'umi/router'
import styles from './MessageList.less'

const Item = List.Item;
const Brief = Item.Brief;

class MessageList extends React.Component{

  state = {
    count:0,
    dataList: [
    ],
    dataLSSPList: [],
    LSSPCount: 0,
    LSSP: false,
    allShow:false,
    allLSSPShow: false,
    pageIndex: 1,
    LSSPPageIndex: 1
  }

  componentDidMount() {
    this.fetchData(1)
    this.fetchLSSPData(1)
  }

  fetchData = (index) => {
    const {dataList} = this.state
    http(`/api/bus/dbxx?${stringify({pageIndex:index,pageSize:10})}`).then(res => {
      let datas = [...dataList, ...res.data.data]
      const allShow = res.data.data.length > 0
      this.setState({
        dataList:datas,
        count:res.data.count,
        allShow:!allShow
      })
    })
  }

  goDetail = (id,taskId) => {
    router.push({
      pathname:'/messagedill',
      query:{
        id,
        taskId
      }
    })
  }

  goLSSPDetail = (id,taskId) => {
    router.push({
      pathname:'/messagedillview',
      query:{
        id,
        taskId
      }
    })
  }

  fetchLSSPData = (index) => {
    const {dataLSSPList} = this.state
    http(`/api/bus/lssp?${stringify({pageIndex:index,pageSize:10})}`).then(res => {
      let datas = [...dataLSSPList, ...res.data.data]
      const allLSSPShow = res.data.data.length > 0
      this.setState({
        dataLSSPList:datas,
        LSSPCount:res.data.count,
        allLSSPShow:!allLSSPShow
      })
    })
  }

  pageUper = () => {
    const {pageIndex} = this.state
    const index = pageIndex + 1
    this.setState({
      pageIndex:index
    }, () => {
      this.fetchData(index)
    })
  }

  LSSPPagerUper = () => {
    const {LSSPPageIndex} = this.state
    const index = LSSPPageIndex + 1
    this.setState({
      LSSPPageIndex:index
    },() => {
      this.fetchLSSPData(index)
    })
  }

  toggleLSSP = () => {
    const {LSSP} =this.state
    const LSSPstates = !LSSP
    this.setState({
      LSSP:LSSPstates
    })
  }

  render() {
    const {count, dataList, LSSPCount, LSSP, dataLSSPList, allShow, allLSSPShow} = this.state
    return (
      <div style={{backgroundColor:'#fff'}} className={styles.messageList}>

        {!LSSP ? (
          <>
          <List
            style={{border:0}}
            renderHeader={() => {
            return (
              <><span>{`你有${count}项待办`}</span>,<span style={{color:'#449dea'}} onClick={this.toggleLSSP}>{LSSPCount}项已办</span></>
            )
          }}>
            {dataList.length > 0 ? dataList.map(t => {
              return (
                <Item
                  key={t.id}
                  arrow="horizontal"
                  multipleLine
                  onClick={() => {
                    this.goDetail(t.rwid,t.id)
                  }}
                  platform="android"
                >
                  {t.rwname}<Brief>审批环节:{t.lcname} <br /> 开始时间:{t.createTime}</Brief>
                </Item>
              )
            }) : (<div style={{width:'100%',height:'100px',textAlign:'center',fontSize:'14px',color:'#ccc', lineHeight:'100px'}}>暂无待办</div>)}
            <WhiteSpace size="sm" />
            </List>
            {dataList.length > 0 ? (allShow ? ( <div style={{width:'100%',height:30,color:'#333',textAlign:'center',marginTop:'30px'}}>已加载全部</div>) : ( <div style={{width:'100%',height:30,marginTop:'30px',color:'blue',textAlign:'center'}} onClick={this.pageUper}>加载更多</div>)) : null}
          </>
        ) : (
          <>
            <List renderHeader={() => {
              return (
                <><span style={{color:'#449dea'}}  onClick={this.toggleLSSP}>{`你有${count}项待办`}</span>,<span  >{LSSPCount}项已办</span></>
              )
            }} className="my-list">
              {dataLSSPList.length > 0 ? (
                dataLSSPList.map(t => {
                  return (
                    <Item
                      key={t.id}
                      arrow="horizontal"
                      multipleLine
                      onClick={() => {
                        this.goLSSPDetail(t.exeId,t.id)
                      }}
                      platform="android"
                    >
                      {t.Rwname}<Brief>审批环节:{t.taskName} <br /> 办理时间:{t.endTime}</Brief>
                    </Item>
                  )
                })
              ) : (<div style={{width:'100%',height:'100px',textAlign:'center',fontSize:'14px',color:'#ccc', lineHeight:'100px'}}>暂无待办</div>)}
              </List>
              {dataLSSPList.length > 0 ? (allLSSPShow ? ( <div style={{width:'100%',height:30,color:'#333',textAlign:'center',marginTop:'30px'}}>已加载全部</div>) : ( <div style={{width:'100%',height:30,marginTop:'30px',color:'blue',textAlign:'center'}} onClick={this.LSSPPagerUper}>加载更多</div>)):null}
          </>
        )}

      </div>
    );
  }

}

export default MessageList

