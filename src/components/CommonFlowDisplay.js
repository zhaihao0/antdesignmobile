import React from 'react'
import { List } from 'antd-mobile';
import http from '@/utils/http'

const Item = List.Item;

class CommonFlowDisplay extends React.Component{

  state = {
    infoList: []
  }

  componentDidMount () {
    const {url} = this.props
    http(url).then(res => {
      if(res.data) {
        this.setState({
          infoList: res.data
        })
      }
    })
  }

  render () {
    const {infoList} =this.state
    return (
      <List>
        {infoList.map(t => {
          return (
            <Item key={t.id} extra={t.value}>{t.key}</Item>
          )
        })}
      </List>
    )
  }
}

export default CommonFlowDisplay
