import React from 'react'
import { List, Modal, SearchBar } from 'antd-mobile';
import styles from './FloatingSearch.less'

const Item = List.Item;

class FloatingSearch extends React.Component {

    state = {
        modalVisible: false,
        testDocs: [],
        selectedText: ''
    }

    onClose = () => {
        this.setState({
            modalVisible: false
        })
    }

    searchChange = val => {
        const testList = [
            { name: '張1三' },
            { name: '張2' },
            { name: '張3' },
            { name: '張4' },
            { name: '張5三' },
            { name: '張6三' },
            { name: '張7' },
            { name: '張7q' },
        ]
        const testlist = testList.filter(t => {
            return t.name.indexOf(val) !== -1
        })
        this.setState({
            testDocs: testlist
        })
    }

    searchSubmit = val => {
        console.log(val)
    }

    render() {

        const { modalVisible, testDocs, selectedText } = this.state
        return (
            <div className={styles.FloatingSearch}>
                <Modal
                    popup
                    visible={modalVisible}
                    onClose={this.onClose}
                    animationType="slide-up"
                >
                    <div className={styles.content} style={{ height: window.innerHeight }}>
                        <SearchBar placeholder="请输入搜索" showCancelButton onCancel={this.onClose} onChange={this.searchChange} onSubmit={this.searchSubmit} />
                        <List>
                            {testDocs.map(t => {
                                return (
                                    <Item key={t.name} platform='android' arrow="horizontal" onClick={() => {

                                        this.setState({
                                            modalVisible: false,
                                            selectedText: t.name,
                                            testDocs: []
                                        })

                                    }}>{t.name}</Item>
                                )
                            })}

                        </List>
                    </div>
                </Modal>
                <Item platform='android' extra={selectedText} arrow="horizontal" onClick={() => {
                    this.setState({
                        modalVisible: true
                    })

                }}>Title</Item>
            </div>
        )
    }
}

export default FloatingSearch