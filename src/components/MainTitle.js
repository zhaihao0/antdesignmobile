import React from 'react'
import { WingBlank, WhiteSpace } from 'antd-mobile';
import styles from './MainTitle.less'

class MainTitle extends React.Component{
  render () {
    const {title} = this.props
    return (
     <>
       <div className={MainTitle}>
         <WhiteSpace size="xl" />
           <WingBlank>
              <div className={styles.verticalMark}> </div>
              <div className={styles.titles}>{title}</div>
           </WingBlank>
         <WhiteSpace size="lg" />
       </div>
     </>
    )
  }
}

export default MainTitle
