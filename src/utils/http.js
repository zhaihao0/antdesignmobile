import axios from 'axios'
import router from 'umi/router'
import { Toast, Modal, } from 'antd-mobile';

const alert = Modal.alert;

// 创建axios实例
const instance = axios.create({
  withCredentials: true,
  // 请求超时时间
  timeout: 50000
})

instance.interceptors.request.use(async config => {
  Toast.loading('请稍后...');
  return config;
}, error => {
  // loadinginstace.close()
  Toast.hide();
  alert('加载超时')
  return Promise.reject(error)
});


// respone拦截器
instance.interceptors.response.use((resp) => {
  Toast.hide();
  return resp;
}, (error) => {
  Toast.hide();
  if (error.response) {
    switch (error.response.status) {
      //未登录
      case 401:
        router.push('/login');
        break;
      //已登录但没有权限
      case 403:
        alert({
          message: "权限不足",
          type: 'error',
          duration: 5 * 1000
        });
        break;
      //没有找到页面
      case 404:
        alert({
          message: "未找到相关内容",
          type: 'error',
          duration: 5 * 1000
        });
        break;
      //后台报错
      case 400:
        alert('400')
        break
      case 422:
        alert({
          message: error.response.data.message,
          type: 'info',
          customClass: "http-error-class",
          iconClass: "el-icon-info",
          duration: 5 * 1000
        });
        break;
      default:
        alert({
          message: "服务错误!",
          type: 'info',
          customClass: "http-error-class",
          iconClass: "el-icon-info",
          duration: 5 * 1000
        });
        break;
    }
  }
  return Promise.reject(error)
})

export default instance
