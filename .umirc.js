import routesList from './src/router'

const {routes} = routesList

export default {
  treeShaking: true,
  plugins: [
    // ref: https://umijs.org/plugin/umi-plugin-react.html
    ['umi-plugin-react', {
      antd: false,
      dva: {
        immer: true, //default:false
        dynamicImport: {
          webpackChunkName: true,
          loadingComponent: './components/Loading.js', // 配置动态加载model
          hmr: true
        }
      },
      dynamicImport: {  // 配置动态加载
        webpackChunkName: true,
        loadingComponent: './components/Loading.js',
        level: 1
      },
      title: 'app',
      dll: true,
      locale: {
        enable: true,
        default: 'en-US',
      },
      routes,
      esLint: true,
      autoprefixer : {
        browsers : [
          "iOS >= 8" ,
          "Android >= 4"
        ]
      } ,
    }],
  ],
  extraBabelPlugins: [
    ['import', { libraryName: "antd-mobile", style: true }],
  ],
  proxy: {
    "/api": {
      "target": "http://192.168.10.245:8888/",
      "changeOrigin": true,
    },
  },
}
